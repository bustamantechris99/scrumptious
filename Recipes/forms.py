from Recipes.models import Recipe
from django.forms import ModelForm
from django import forms


class RecipeForm(ModelForm):
    title = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-title', 'placeholder':'Recipe Name'}))
    picture = forms.URLField(widget=forms.TextInput(attrs={'class': 'form-title', 'placeholder':'Picture Url'}))
    calories = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-title', 'placeholder':'Amount of Calories'}))
    description = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-title', 'placeholder':'Description of your recipe!'}))
    class Meta:
        model = Recipe
        fields = ["title", 'picture', 'calories', 'description']