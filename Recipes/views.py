from django.shortcuts import render, get_object_or_404, redirect
from Recipes.models import Recipe
from Recipes.forms import RecipeForm
# from django.http import htt

def show_recipe(request):
    # return HttpResponse("<h1>This view is working</h1>\n<h2>This is really cool and I am glad I am starting to understand it </h2>")
    recipes = Recipe.objects.all()
    context = { 'recipes' : recipes
              }
    # for recipe in recipes:
    #   context = {
    #     'title': recipe.title,
    #     'image': recipe.picture,
    #     'calories': recipe.calories,
    #     'description': recipe.description,
    #     'date': recipe.created_on

    #   }
    return render(request, 'recipes/detail.html', context)
def show_list(request, id):
  recipe = get_object_or_404(Recipe, id=id)
  context = {
            'recipe': recipe}
          # 'title': recipe.title,
          # 'image': recipe.picture,
          # 'calories': recipe.calories,
          # 'description': recipe.description
  return render(request, 'recipes/list.html/', context)

def create_recipe(request):
  if request.method == 'POST':
      form = RecipeForm(request.POST)
      if form.is_valid():
          form.save()
          return redirect("show_recipe")
  else:
    form = RecipeForm()
    context = {
            'form' : form
          }
  return render(request, 'recipes/create.html', context)

def edit_recipe(request, id):
    post = get_object_or_404(Recipe, id=id)
    if request.method =='POST':
      form = RecipeForm(request.POST, instance = post)
      if form.is_valid():
        form.save()
        return redirect('show_list', id=id)
    else:
        form = RecipeForm(instance = post)
        context = {
          'post_object': post,
          'post_form': form}
    return render(request, 'recipes/edit.html', context)
