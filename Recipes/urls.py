from django.urls import path
from Recipes.views import show_recipe, show_list, create_recipe, edit_recipe

urlpatterns = [
    path('recipes/', show_recipe, name='show_recipe'),
    path('recipes/list.html/<id>/', show_list, name = 'show_list'),
    path('recipes/create.html/', create_recipe, name='create_recipe'),
    path('recipes/edit/<id>/', edit_recipe, name='edit_recipe'),
]
