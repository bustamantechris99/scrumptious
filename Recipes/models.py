from django.db import models

class Recipe(models.Model):
    title = models.CharField(max_length=200)
    picture = models.URLField()
    description = models.TextField(max_length= 50)
    created_on = models.DateTimeField(auto_now_add=True)
    calories = models.BigIntegerField(null=True)
    have_made = models.BooleanField(default=False)

    # def __str__(self):
    #     return self.title
